using System.Threading.Tasks;
using KPlatformCA.Application.Common.Models;

namespace KPlatformCA.Application.Common.Interfaces
{
    public interface IIdentityRoleService
    {
        Task<(Result result, string RoleId)> CreateRoleAsync(string roleName);
    }
}