﻿using KPlatformCA.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace KPlatformCA.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
