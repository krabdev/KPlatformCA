﻿using KPlatformCA.Application.Common.Mappings;
using KPlatformCA.Domain.Entities;

namespace KPlatformCA.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
