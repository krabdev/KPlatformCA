using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KPlatformCA.Application.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace KPlatformCA.Application.ApplicationRoles.Queries.GetRoles
{
    public class GetRolesQuery : IRequest<RolesVm>
    {
        public class GetRolesQueryHandler : IRequestHandler<GetRolesQuery, RolesVm>
        {
            private readonly RoleManager<ApplicationRole> _roleManager;
            private readonly IMapper _mapper;

            public GetRolesQueryHandler(RoleManager<ApplicationRole> roleManager,
                IMapper mapper)
            {
                _roleManager = roleManager;
                _mapper = mapper;
            }
            
            public async Task<RolesVm> Handle(GetRolesQuery request, CancellationToken cancellationToken)
            {
                var vm = new RolesVm();
                vm.Roles = await _roleManager
                    .Roles
                    .ProjectTo<ApplicationRoleDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
                return vm;
            }
        }
    }
}