using KPlatformCA.Application.Common.Mappings;
using KPlatformCA.Application.Entities;

namespace KPlatformCA.Application.ApplicationRoles.Queries.GetRoles
{
    public class ApplicationRoleDto : IMapFrom<ApplicationRole>
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}