using System.Collections.Generic;

namespace KPlatformCA.Application.ApplicationRoles.Queries.GetRoles
{
    public class RolesVm
    {
        public RolesVm()
        {
            Roles = new List<ApplicationRoleDto>();
        }
        public IList<ApplicationRoleDto> Roles { get; set; }
    }
}