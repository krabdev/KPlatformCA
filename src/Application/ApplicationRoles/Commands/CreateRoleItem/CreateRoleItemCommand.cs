using System.Threading;
using System.Threading.Tasks;
using KPlatformCA.Application.Common.Interfaces;
using MediatR;

namespace KPlatformCA.Application.ApplicationRoles.Commands.CreateRoleItem
{
    public class CreateRoleItemCommand : IRequest<string>
    {
        public string RoleName { get; set; }

        public class CreateRoleItemCommandHandler : IRequestHandler<CreateRoleItemCommand, string>
        {
            private readonly IIdentityRoleService _roleService;

            public CreateRoleItemCommandHandler(IIdentityRoleService roleService)
            {
                _roleService = roleService;
            }

            public async Task<string> Handle(CreateRoleItemCommand request, CancellationToken cancellationToken)
            {
                var r = await _roleService.CreateRoleAsync(request.RoleName);
                return r.RoleId;
            }
        }
    }
}