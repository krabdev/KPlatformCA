using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KPlatformCA.Application.ApplicationRoles.Queries.GetRoles;
using KPlatformCA.Application.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace KPlatformCA.Application.ApplicationUsers.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<UsersVm>
    {
        public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, UsersVm>
        {
            private readonly UserManager<ApplicationUser> _userManager;
            private readonly RoleManager<ApplicationRole> _roleManager;
            private readonly IMapper _mapper;

            public GetUsersQueryHandler(UserManager<ApplicationUser> userManager,
                RoleManager<ApplicationRole> roleManager,
                IMapper mapper)
            {
                _userManager = userManager;
                _roleManager = roleManager;
                _mapper = mapper;
            }
            
            public async Task<UsersVm> Handle(GetUsersQuery request, CancellationToken cancellationToken)
            {
                var vm = new UsersVm();
                
                var users = await _userManager
                    .Users
                    .Include(x => x.Roles)
                    //.ProjectTo<ApplicationUserDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
                foreach (var user in users)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    vm.Users.Add(new ApplicationUserDto
                    {
                        RoleNames = roles,
                        Id = user.Id,
                        UserName = user.UserName,
                        Email = user.Email
                    });
                }
                return vm;
            }
        }
    }
}