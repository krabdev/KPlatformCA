using System.Collections.Generic;

namespace KPlatformCA.Application.ApplicationUsers.Queries.GetUsers
{
    public class UsersVm
    {
        public UsersVm()
        {
            Users = new List<ApplicationUserDto>();
        }

        public IList<ApplicationUserDto> Users { get; set; }
    }
}