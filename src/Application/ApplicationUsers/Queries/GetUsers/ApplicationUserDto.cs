using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using KPlatformCA.Application.ApplicationRoles.Queries.GetRoles;
using KPlatformCA.Application.Common.Mappings;
using KPlatformCA.Application.Entities;

namespace KPlatformCA.Application.ApplicationUsers.Queries.GetUsers
{
    public class ApplicationUserDto : IMapFrom<ApplicationUser>
    {
        public ApplicationUserDto()
        {
            RoleNames = new List<string>();
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public IList<string> RoleNames { get; set; }
    }
}