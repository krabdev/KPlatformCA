﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KPlatformCA.Application.Common.Exceptions;
using KPlatformCA.Application.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace KPlatformCA.Application.ApplicationUsers.Commands.RemoveRoleFromUser
{
    public class RemoveUserRoleCommand : IRequest<bool>
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }

        public class RemoveUserRoleCommandHandler : IRequestHandler<RemoveUserRoleCommand, bool>
        {
            private readonly UserManager<ApplicationUser> _userManager;
            private readonly RoleManager<ApplicationRole> _roleManager;

            public RemoveUserRoleCommandHandler(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
            {
                _userManager = userManager;
                _roleManager = roleManager;
            }

            public async Task<bool> Handle(RemoveUserRoleCommand request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByIdAsync(request.UserId);
                if (user == null)
                {
                    throw new NotFoundException("user", request.UserId);
                }

                var role = await _roleManager.FindByIdAsync(request.RoleId);
                if (role == null)
                {
                    throw new NotFoundException("role", request.RoleId);
                }

                var result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                return result.Succeeded;
            }
        }
    }
}
