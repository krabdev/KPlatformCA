﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace KPlatformCA.Application.ApplicationUsers.Commands.RemoveRoleFromUser
{
    public class RemoveUserRoleCommandValidator : AbstractValidator<RemoveUserRoleCommand>
    {
        public RemoveUserRoleCommandValidator()
        {
            RuleFor(x => x.RoleId).NotNull().NotEmpty();
            RuleFor(x => x.UserId).NotNull().NotEmpty();
        }
    }
}
