using System.Threading;
using System.Threading.Tasks;
using KPlatformCA.Application.Common.Exceptions;
using KPlatformCA.Application.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace KPlatformCA.Application.ApplicationUsers.Commands.ChangePassword
{
    public class ChangePasswordCommand : IRequest<bool>
    {
        public string UserId { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }

        public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, bool>
        {
            private readonly UserManager<ApplicationUser> _userManager;

            public ChangePasswordCommandHandler(UserManager<ApplicationUser> userManager)
            {
                _userManager = userManager;
            }
            
            public async Task<bool> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByIdAsync(request.UserId);
                if (user == null)
                {
                    throw new NotFoundException("User", request.UserId);
                }

                var changePasswordResult =
                    await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
                if (!changePasswordResult.Succeeded)
                {
                    return false;
                }

                return true;
            }
        }
    }
}