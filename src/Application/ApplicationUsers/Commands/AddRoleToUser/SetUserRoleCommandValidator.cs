﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using KPlatformCA.Application.ApplicationUsers.Commands.AddRoleToUser;

namespace KPlatformCA.Application.ApplicationUsers.Commands.SetUserRole
{
    class SetUserRoleCommandValidator : AbstractValidator<SetUserRoleCommand>
    {
        public SetUserRoleCommandValidator()
        {
            RuleFor(x => x.RoleId)
                .NotEmpty()
                .NotNull();

            RuleFor(x => x.UserId)
                .NotNull()
                .NotEmpty();
        }
    }
}
