﻿using System.Threading;
using System.Threading.Tasks;
using KPlatformCA.Application.Common.Exceptions;
using KPlatformCA.Application.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace KPlatformCA.Application.ApplicationUsers.Commands.AddRoleToUser
{
    public class SetUserRoleCommand : IRequest<bool>
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }

        public class SetUserRoleCommandHandler : IRequestHandler<SetUserRoleCommand, bool>
        {
            private readonly UserManager<ApplicationUser> _userManager;
            private readonly RoleManager<ApplicationRole> _roleManager;

            public SetUserRoleCommandHandler(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
            {
                _userManager = userManager;
                _roleManager = roleManager;
            }

            public async Task<bool> Handle(SetUserRoleCommand request, CancellationToken cancellationToken)
            {
                var role = await _roleManager.FindByIdAsync(request.RoleId);
                if (role == null)
                {
                    throw new NotFoundException("Role", request.RoleId);
                }

                var user = await _userManager.FindByIdAsync(request.UserId);
                if (user == null)
                {
                    throw new NotFoundException("User", request.UserId);
                }

                var result = await _userManager.AddToRoleAsync(user, role.Name);
                return result.Succeeded;
            }
        }
    }
}
