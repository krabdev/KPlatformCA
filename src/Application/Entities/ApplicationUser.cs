﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace KPlatformCA.Application.Entities
{
    public class ApplicationUser : IdentityUser
    {
    
        public virtual ICollection<IdentityUserRole<string>> Roles { get; } = new List<IdentityUserRole<string>>();
    }
}
