using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace KPlatformCA.Application.Entities
{
    public class ApplicationRole : IdentityRole
    {
        public virtual ICollection<IdentityUserRole<string>> Users { get; } = new List<IdentityUserRole<string>>();
    }
}