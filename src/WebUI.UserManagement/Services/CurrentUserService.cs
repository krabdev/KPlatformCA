using System.Security.Claims;
using KPlatformCA.Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace WebUI.UserManagement.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public string UserId { get; }
    }
}