using System.Threading.Tasks;
using KPlatformCA.Application.ApplicationUsers.Commands.AddRoleToUser;
using KPlatformCA.Application.ApplicationUsers.Commands.ChangePassword;
using KPlatformCA.Application.ApplicationUsers.Commands.RemoveRoleFromUser;
using KPlatformCA.Application.ApplicationUsers.Commands.SetUserRole;
using KPlatformCA.Application.ApplicationUsers.Queries.GetUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.UserManagement.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
        [HttpGet]
        public async Task<UsersVm> GetUsers()
        {
            var query = new GetUsersQuery();
            return await Mediator.Send(query);
        }

        [HttpPost]
        public async Task<bool> ChangePassword(ChangePasswordCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPost]
        public async Task<bool> AddRoleToUser(SetUserRoleCommand command) => await Mediator.Send(command);

        [HttpDelete]
        public async Task<bool> RemoveRoleFromUser(RemoveUserRoleCommand command) => await Mediator.Send(command);
    }
}