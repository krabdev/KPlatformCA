using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KPlatformCA.Application.ApplicationRoles.Commands.CreateRoleItem;
using KPlatformCA.Application.ApplicationRoles.Queries.GetRoles;
using KPlatformCA.Application.Common.Interfaces;
using KPlatformCA.Application.Entities;
using KPlatformCA.Domain.Entities;
using KPlatformCA.Infrastructure.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebUI.UserManagement.Controllers
{
    [Authorize]
    public class RoleController : ApiController
    {
        private readonly IIdentityRoleService _roleService;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;

        public RoleController(IIdentityRoleService roleService,
            RoleManager<ApplicationRole> roleManager,
            IMapper mapper)
        {
            _roleService = roleService;
            _roleManager = roleManager;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<string>> CreateRole(CreateRoleItemCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpGet]
        public async Task<RolesVm> GetRoles()
        {
            var query = new GetRolesQuery();
            return await Mediator.Send(query);
        }
    }
}