using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KPlatformCA.Application.Common.Interfaces;
using KPlatformCA.Application.Common.Models;
using KPlatformCA.Application.Entities;
using KPlatformCA.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace KPlatformCA.Infrastructure.Identity
{
    public class IdentityRoleService : IIdentityRoleService
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;

        public IdentityRoleService(RoleManager<ApplicationRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _mapper = mapper;
        }
        
        public async Task<(Result result, string RoleId)> CreateRoleAsync(string roleName)
        {
            var newApplicationRole = new ApplicationRole()
            {
                Name = roleName,
                NormalizedName = roleName
            };
            var result = await _roleManager.CreateAsync(newApplicationRole);
            
            return (result.ToApplicationResult(), newApplicationRole.Id);
        }
    }
}