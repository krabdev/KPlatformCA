﻿using KPlatformCA.Application.Common.Interfaces;
using System;

namespace KPlatformCA.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
