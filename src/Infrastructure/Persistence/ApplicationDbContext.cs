﻿using System;
using KPlatformCA.Application.Common.Interfaces;
using KPlatformCA.Domain.Common;
using KPlatformCA.Domain.Entities;
using KPlatformCA.Infrastructure.Identity;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Interfaces;
using KPlatformCA.Application.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

// namespace KPlatformCA.Infrastructure.Persistence
// {
//     public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>, IApplicationDbContext
//     {
//         private readonly ICurrentUserService _currentUserService;
//         private readonly IDateTime _dateTime;
//
//         public ApplicationDbContext(
//             DbContextOptions options,
//             IOptions<OperationalStoreOptions> operationalStoreOptions,
//             ICurrentUserService currentUserService,
//             IDateTime dateTime) : base(options, operationalStoreOptions)
//         {
//             _currentUserService = currentUserService;
//             _dateTime = dateTime;
//         }
//
//         public DbSet<TodoList> TodoLists { get; set; }
//         public DbSet<TodoItem> TodoItems { get; set; }
//
//         public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
//         {
//             foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
//             {
//                 switch (entry.State)
//                 {
//                     case EntityState.Added:
//                         entry.Entity.CreatedBy = _currentUserService.UserId;
//                         entry.Entity.Created = _dateTime.Now;
//                         break;
//                     case EntityState.Modified:
//                         entry.Entity.LastModifiedBy = _currentUserService.UserId;
//                         entry.Entity.LastModified = _dateTime.Now;
//                         break;
//                 }
//             }
//
//             return base.SaveChangesAsync(cancellationToken);
//         }
//
//         protected override void OnModelCreating(ModelBuilder builder)
//         {
//             builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
//
//             base.OnModelCreating(builder);
//         }
//     }
// }

namespace KPlatformCA.Infrastructure.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IPersistedGrantDbContext, IApplicationDbContext
        
        
        //ApiAuthorizationDbContext<ApplicationUser>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;
        private readonly IOptions<OperationalStoreOptions> _operationalStoreOptions;

        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
            ICurrentUserService currentUserService,
            IDateTime dateTime) : base(options)
        {
            _currentUserService = currentUserService;
            _dateTime = dateTime;
            _operationalStoreOptions = operationalStoreOptions;
        }

        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }
        
        public DbSet<PersistedGrant> PersistedGrants { get; set; }

        public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }

        Task<int> IPersistedGrantDbContext.SaveChangesAsync() => base.SaveChangesAsync();

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.ConfigurePersistedGrantContext(_operationalStoreOptions.Value);
            base.OnModelCreating(builder);
            
            builder.Entity<ApplicationRole>().HasData(
                new ApplicationRole { Name = "Admin", NormalizedName = "Admin".ToUpper()}
            );


            builder.Entity<ApplicationUser>()
                .HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<ApplicationRole>()
                .HasMany(e => e.Users)
                .WithOne()
                .HasForeignKey(e => e.RoleId)
                .IsRequired();
        }
    }
}
