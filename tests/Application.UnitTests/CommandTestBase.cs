using KPlatformCA.Infrastructure.Persistence;
using System;
using KPlatformCA.Application.Common.Interfaces;

namespace KPlatformCA.Application.UnitTests.Common
{
    public class CommandTestBase : IDisposable
    {
        public CommandTestBase()
        {
            Context = ApplicationDbContextFactory.Create();
        }

        public ApplicationDbContext Context { get; }

        public void Dispose()
        {
            ApplicationDbContextFactory.Destroy(Context);
        }
    }
}