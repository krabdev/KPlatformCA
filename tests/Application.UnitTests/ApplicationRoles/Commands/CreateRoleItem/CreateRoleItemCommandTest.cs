// using System;
// using System.Threading;
// using System.Threading.Tasks;
// using KPlatformCA.Application.ApplicationRoles.Commands.CreateRoleItem;
// using KPlatformCA.Application.Common.Interfaces;
// using KPlatformCA.Application.UnitTests.Common;
// using Xunit;
//
// namespace KPlatformCA.Application.UnitTests.ApplicationRoles.Commands.CreateRoleItem
// {
//     public class CreateRoleItemCommandTest : CommandTestBase
//     {
//         private readonly IIdentityRoleService _roleService;
//
//         public CreateRoleItemCommandTest(IIdentityRoleService roleService)
//         {
//             _roleService = roleService;
//         }
//         
//         [Fact]
//         public async Task Handle_ShouldPersistRole()
//         {
//             var command = new CreateRoleItemCommand()
//             {
//                 RoleName = "testrole"
//             };
//             var handler = new CreateRoleItemCommand.CreateRoleItemCommandHandler(_roleService);
//             var result = await handler.Handle(command, CancellationToken.None);
//             Console.WriteLine(result);
//         }
//     }
// }