using System;
using System.Threading.Tasks;
using KPlatformCA.Application;
using KPlatformCA.Application.Common.Interfaces;
using KPlatformCA.Application.Common.Models;

namespace WebUI.UserManagement.IntegrationTests
{
    public class TestIdentityService : IIdentityService
    {
        public Task<string> GetUserNameAsync(string userId)
        {
            return Task.FromResult("jason@clean-architecture");
        }

        public Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public Task<Result> DeleteUserAsync(string userId)
        {
            throw new NotImplementedException();
        }
    }
}