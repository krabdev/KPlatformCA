using System.Threading.Tasks;
using KPlatformCA.Application.ApplicationRoles.Commands.CreateRoleItem;
using Xunit;

namespace WebUI.UserManagement.IntegrationTests.Roles
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidCreateTodoItemCommand_ReturnsSuccessCode()
        {
            var client = await _factory.GetAuthenticatedClientAsync();
            var command = new CreateRoleItemCommand
            {
                RoleName = "testrole"
            };
            var content = IntegrationTestHelper.GetRequestContent(command);
            var response = await client.PostAsync($"/api/Role/CreateRole", content);
            response.EnsureSuccessStatusCode();
        }
    }
}