using System;
using KPlatformCA.Application.Common.Interfaces;

namespace WebUI.UserManagement.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}