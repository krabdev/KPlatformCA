﻿using KPlatformCA.Application.Common.Interfaces;
using System;

namespace KPlatformCA.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
